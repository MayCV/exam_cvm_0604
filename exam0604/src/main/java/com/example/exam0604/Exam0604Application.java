package com.example.exam0604;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exam0604Application {

    public static void main(String[] args) {
        SpringApplication.run(Exam0604Application.class, args);
    }

}
